require "rails_helper"

RSpec.describe "VisitorsSearchName" do
  describe "#GET index" do
    subject(:request) do
      get "/chains/name/search/#{chain_name}", headers: headers
    end

    before do
      create(:chain, name: "Rede")
      Chain.reindex
    end

    include_context "authenticated user"

    let(:chain_name) { "Rede" }
    let(:response_json) { JSON.parse(response.body) }

    context "when the name is equal to chain name" do
      it "returns chain" do
        request
        expect(response_json.first["name"]).to eq("Rede")
      end
    end

    context "when the name is similar to chain name" do
      let(:visitor_name) { "Rde" }
      it "returns chain" do
        request
        expect(response_json.first["name"]).to eq("Rede")
      end
    end

    context "when the name is different to chain name" do
      let(:chain_name) { "Teste" }

      it "doesn't return records" do
        request
        expect(response_json.count).to be_zero
      end
    end
  end
end
