require "rails_helper"

RSpec.describe "Chains" do
  include_context "authenticated user"

  let(:response_json) { JSON.parse(response.body) }
  let(:klass) { Chain }
  let(:expected_json_object) { Chain.first.slice(:id, :name, :cnpj) }
  let(:validation_object_key) { "cnpj" }

  describe "#GET index" do
    subject(:request) { get "/chains", headers: headers }

    before do
      create(:chain)
      create(:chain)
    end

    it "returns two records" do
      request
      expect(response_json.count).to eq(2)
    end
  end

  describe "#GET show" do
    subject(:request) { get "/chains/#{chain_id}", headers: headers }

    let(:chain_id) { create(:chain).id }

    include_examples "returns json object"
  end

  describe "#POST create" do
    subject(:request) { post "/chains", headers: headers, params: params }
    let(:params) do
      {
        chain: {
          name: "test",
          cnpj: "11111111111111"
        }
      }
    end

    context "when the params are correct" do
      include_examples "create method with required params"
    end

    context "when the params are incomplete" do
      before { params[:chain].except!(:cnpj) }

      let(:validation_messages) do
        [
          "não pode ficar em branco",
          "não possui o tamanho esperado (14 caracteres)"
        ]
      end

      include_examples "create method with incomplete params"
    end
  end

  describe "#PUT update" do
    subject(:request) { put "/chains/#{chain.id}", headers: headers, params: params }

    let(:params) { { chain: { name: updated_value } } }
    let(:chain) { create(:chain) }
    let(:entity) { chain.reload }
    let(:updated_attribute) { "name" }
    let(:updated_value) { "test" }

    context "when the params are correct" do
      include_examples "update method with required params"
    end

    context "when the params are invalids" do
      before { params[:chain].merge!({ cnpj: "111111111111111" }) }

      let(:validation_messages) { ["não possui o tamanho esperado (14 caracteres)"] }

      include_examples "update method with invalid params"
    end
  end

  describe "#DELETE destroy" do
    subject(:request) { delete "/chains/#{chain_id}", headers: headers }

    before { create(:chain) }

    let(:chain_id) { Chain.first.id }

    include_examples "destroy method"
  end
end

