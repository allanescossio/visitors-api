require "rails_helper"

RSpec.describe "Visitors" do
  include_context "authenticated user"

  let(:response_json) { JSON.parse(response.body) }
  let(:klass) { Visitor }
  let(:expected_json_object) do
    visitor = Visitor.first
    chain = visitor.chain
    visitor.slice(:id, :name).merge({ chain: chain.slice(:id, :name, :cnpj) })
  end
  let(:validation_object_key) { "name" }
  let(:validation_messages) { ["não pode ficar em branco"] }

  describe "#GET index" do
    subject(:request) { get "/visitors", headers: headers }

    before do
      create(:visitor)
      create(:visitor)
    end

    it "returns two records" do
      request
      expect(response_json.count).to eq(2)
    end
  end

  describe "#GET show" do
    subject(:request) { get "/visitors/#{visitor_id}", headers: headers }

    let(:visitor_id) { create(:visitor).id }

    include_examples "returns json object"
  end

  describe "#POST create" do
    subject(:request) { post "/visitors", headers: headers, params: params }
    let(:params) do
      {
        visitor: {
          name: "test",
          chain_id: create(:chain).id
        }
      }
    end

    context "when the params are correct" do
      include_examples "create method with required params"
    end

    context "when the params are incomplete" do
      before { params[:visitor].except!(:name) }

      include_examples "create method with incomplete params"
    end
  end

  describe "#PUT update" do
    subject(:request) { put "/visitors/#{visitor.id}", headers: headers, params: params }

    let(:visitor) { create(:visitor) }
    let(:params) { { visitor: { name: updated_value } } }
    let(:entity) { visitor.reload }
    let(:updated_attribute) { "name" }
    let(:updated_value) { "test" }

    context "when the params are correct" do
      include_examples "update method with required params"
    end

    context "when the params are invalids" do
      before { params[:visitor].merge!({ name: "" }) }

      include_examples "update method with invalid params"
    end
  end

  describe "#DELETE destroy" do
    subject(:request) { delete "/visitors/#{visitor_id}", headers: headers }

    before { create(:visitor) }

    let(:visitor_id) { Visitor.first.id }

    include_examples "destroy method"
  end
end

