require "rails_helper"

RSpec.describe "VisitorsSearchName" do
  describe "#GET index" do
    subject(:request) do
      get "/visitors/name/search/#{visitor_name}", headers: headers 
    end

    before do
      create(:visitor, name: "Visitante")
      Visitor.reindex
    end

    include_context "authenticated user"

    let(:visitor_name) { "Visitante" }
    let(:response_json) { JSON.parse(response.body) }

    context "when the name is equal to visitor name" do
      it "returns visitor" do
        request
        expect(response_json.first["name"]).to eq("Visitante")
      end
    end

    context "when the name is similar to visitor name" do
      let(:visitor_name) { "Viitante" }
      it "returns visitor" do
        request
        expect(response_json.first["name"]).to eq("Visitante")
      end
    end

    context "when the name is different to visitor name" do
      let(:visitor_name) { "Teste" }

      it "doesn't return records" do
        request
        expect(response_json.count).to be_zero
      end
    end
  end
end
