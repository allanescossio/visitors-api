RSpec.shared_examples "returns json object" do
  it "returns json object" do
    request
    expect(response_json).to eq(expected_json_object)
  end
end

RSpec.shared_examples "create method with required params" do
  it "creates record" do
    expect { request }.to change(klass, :count).by(1)
  end

  include_examples "returns json object"

  it "returns :created http status" do
    request
    expect(response).to have_http_status(:created)
  end
end

RSpec.shared_examples "create method with incomplete params" do
  it "returns :unprocessable_entity http status" do
    request
    expect(response).to have_http_status(:unprocessable_entity)
  end

  it "doesn't create record" do
    expect { request }.not_to change(klass, :count)
  end

  it "returns validations messages" do
    request
    expect(response_json[validation_object_key]).to match_array(validation_messages)
  end
end

RSpec.shared_examples "update method with required params" do
  it "updates record" do
    request
    expect(entity.attributes[updated_attribute]).to eq(updated_value)
  end

  it "returns json object" do
    request
    expect(response_json).to eq(expected_json_object)
  end

  it "returns :ok http status" do
    request
    expect(response).to have_http_status(:ok)
  end

  RSpec.shared_examples "update method with invalid params" do
    it "returns :unprocessable_entity http status" do
      request
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it "doesn't update record" do
      request
      expect(klass.find(entity.id)).to eq(entity)
    end

    it "returns validations messages" do
      request
      expect(response_json[validation_object_key]).to match_array(validation_messages)
    end
  end

  RSpec.shared_examples "destroy method" do
    it "removes record" do
      expect { request }.to change(klass, :count).by(-1)
    end

    it "returns :no_content http status" do
      request
      expect(response).to have_http_status(:no_content)
    end
  end
end
