RSpec.shared_context "authenticated user" do
  let(:user) { create(:user, login: "test", password: "123456") }
  let(:token) do
    payload = {
      user_id: user.id,
      token_type: 'client_a2',
      exp: 1.hours.from_now.to_i
    }
    JWT.encode payload, Rails.application.credentials[:secret_jwt_auth], "HS256"
  end
  let(:headers) { { "Authorization": token } }
end
