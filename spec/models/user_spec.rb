require 'rails_helper'

RSpec.describe User do
  it { is_expected.to belong_to(:chain).optional  }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:login) }
  it { is_expected.to validate_presence_of(:password) }
end
