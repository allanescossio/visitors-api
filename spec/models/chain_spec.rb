require 'rails_helper'

RSpec.describe Chain do
  it { is_expected.to have_many(:users).dependent(:destroy) }
  it { is_expected.to have_many(:visitors).dependent(:destroy) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:cnpj) }
  it { is_expected.to validate_length_of(:cnpj).is_equal_to(14) }
end
