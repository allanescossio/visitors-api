require 'rails_helper'

RSpec.describe VisitorMovement do
  it { is_expected.to belong_to(:visitor) }
end
