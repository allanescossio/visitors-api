require 'rails_helper'

RSpec.describe Visitor do
  it { is_expected.to belong_to(:chain) }
  it { is_expected.to have_many(:visitor_movements).dependent(:destroy) }
  it { is_expected.to validate_presence_of(:name) }
end
