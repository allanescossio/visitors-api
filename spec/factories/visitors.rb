FactoryBot.define do
  factory :visitor do
    association :chain, factory: :chain
    name { Faker::Name.name }
  end
end
