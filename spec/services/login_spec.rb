require "rails_helper"

RSpec.describe Login do
  describe "#call" do
    subject(:call) { described_class.call(user, password) }

    let(:user) { create(:user, login: "test", password: "123456") }
    let(:password) { '123456' }

    context "when authenticate" do
      it "returns authenticated equals true" do
        expect(call.authenticated?).to be true
      end

      it "returns token" do
        expect(call.token).to be_present
      end
    end

    context "when user is blank" do
      let(:user) { nil }

      it "returns authenticated equals false" do
        expect(call.authenticated?).to be false
      end

      it "returns message error: Dados inválidos" do
        expect(call.error).to eq("Dados inválidos")
      end
    end

    context "when then password is wrong" do
      let(:password) { '123' }

      it "returns authenticated equals false" do
        expect(call.authenticated?).to be false
      end

      it "doesn't return token" do
        expect(call.token).to be_nil
      end

      it "returns message error: Dados inválidos" do
        expect(call.error).to eq("Dados inválidos")
      end
    end
  end
end
