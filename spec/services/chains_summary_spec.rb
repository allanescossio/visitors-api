require "rails_helper"

RSpec.describe ChainsSummary do
  describe "#call" do
    subject(:call) { described_class.call }

    before do
      chain = create(:chain)
      create(:visitor, chain: chain)
      create(:visitor, chain: chain)
    end

    context "when there is chains" do
      it "returns summary" do
        expect(call).to eq(OpenStruct.new(chains_total: 1, visitors_total: 2))
      end
    end
  end
end

