require 'rails_helper'

RSpec.describe AuthRequest do
  describe "#call" do
    subject(:call) { described_class.call(token) }

    let(:user) { create(:user, login: "test", password: "123456") }
    let(:expire) { 1.hours.from_now.to_i }
    let(:token) do
      payload = {
        user_id: user.id,
        token_type: 'client_a2',
        exp: expire
      }
      JWT.encode payload, Rails.application.credentials[:secret_jwt_auth], 'HS256'
    end

    context "when token is valid" do
      it "returns authenticated equals true" do
        expect(call.authenticated?).to be true
      end

      it "returns new token" do
        new_token = call.token
        expect(new_token).to be_present
        expect(new_token).not_to eq(token)
      end
    end

    context "when token is invalid" do
      let(:token) { "" }

      it "returns authenticated equals false" do
        expect(call.authenticated?).to be false
      end

      it "returns message error: Efetue login" do
        expect(call.error).to eq("Efetue login")
      end
    end

    context "when token has expired" do
      let(:expire) { 1.hour.ago.to_i }

      it "returns authenticated equals false" do
        expect(call.authenticated?).to be false
      end

      it "returns message error: Sessão expirada. Efetue o login novamente." do
        expect(call.error).to eq("Sessão expirada. Efetue o login novamente.")
      end
    end
  end
end
