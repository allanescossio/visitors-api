class VisitorMovementsController < ApplicationController
  before_action :set_visitor_movement, only: [:show, :update, :destroy]

  def index
    @visitor_movements = VisitorMovement.all

    render json: @visitor_movements
  end

  def show
    render json: @visitor_movement
  end

  def create
    @visitor_movement = VisitorMovement.new(visitor_movement_params)

    if @visitor_movement.save
      render json: @visitor_movement, status: :created, location: @visitor_movement
    else
      render json: @visitor_movement.errors, status: :unprocessable_entity
    end
  end


  private

  def set_visitor_movement
    @visitor_movement = VisitorMovement.find(params[:id])
  end

  def visitor_movement_params
    params.require(:visitor_movement).permit(:visitor_id, :kind)
  end
end
