class ApplicationController < ActionController::API
  before_action :validate_token

  private

  def validate_token
    token = request.headers['Authorization']
    result = AuthRequest.call(token)
    render json: { error: result.error }, status: :unauthorized unless result.authenticated?
    response.headers['JWT-Token-Renewed'] = result.token
  end
end
