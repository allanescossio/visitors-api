class VisitorsSearchNameController < ApplicationController
  def index
    visitors = Visitor.search(params[:query], fields: [:name])

    render json: visitors
  end
end
