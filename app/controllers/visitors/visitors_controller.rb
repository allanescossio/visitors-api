class VisitorsController < ApplicationController
  before_action :set_visitor, only: [:show, :update, :destroy]

  def index
    @visitors = Visitor.all

    render json: @visitors
  end

  def show
    render json: @visitor
  end

  def create
    @visitor = Visitor.new(visitor_params)

    if @visitor.save
      render json: @visitor, status: :created, location: @visitor
    else
      render json: @visitor.errors, status: :unprocessable_entity
    end
  end

  def update
    if @visitor.update(visitor_params)
      render json: @visitor
    else
      render json: @visitor.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @visitor.destroy
  end

  private

  def set_visitor
    @visitor = Visitor.find(params[:id])
  end

  def visitor_params
    params.require(:visitor).permit(:name, :chain_id, :photo)
  end
end
