class ChainsController < ApplicationController
  before_action :set_chain, only: [:show, :update, :destroy]

  def index
    @chains = Chain.all

    render json: @chains
  end

  def show
    render json: @chain
  end

  def create
    @chain = Chain.new(chain_params)

    if @chain.save
      render json: @chain, status: :created
    else
      render json: @chain.errors, status: :unprocessable_entity
    end
  end

  def update
    if @chain.update(chain_params)
      render json: @chain
    else
      render json: @chain.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @chain.destroy
  end

  private
    def set_chain
      @chain = Chain.find(params[:id])
    end

    def chain_params
      params.require(:chain).permit(:name, :cnpj)
    end
end
