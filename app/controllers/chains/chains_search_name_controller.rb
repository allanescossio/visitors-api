class ChainsSearchNameController < ApplicationController
  def index
    chains = Chain.search(params[:query], fields: [:name])

    render json: chains
  end
end
