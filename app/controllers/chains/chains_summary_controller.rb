class ChainsSummaryController < ApplicationController
  skip_before_action :validate_token

  def index
    summary = ChainsSummary.call

    render json: JSON.parse(summary.to_json)["table"]
  end
end
