class LoginController < ApplicationController
  skip_before_action :validate_token

  def create
    user = User.find_by(login: auth_params[:login])

    result = Login.call(user, auth_params[:password])
    if result.authenticated?
      render json: { token: result.token }
    else
      render json: { error: result.error }, status: :unauthorized
    end
  end

  private

  def auth_params
    params.require(:auth).permit(:login, :password)
  end
end
