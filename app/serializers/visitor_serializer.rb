class VisitorSerializer < ActiveModel::Serializer
  attributes :id, :name, :chain

  def chain
    ActiveModelSerializers::SerializableResource.new(
      object.chain,
      serializer: ChainSerializer
    )
  end
end
