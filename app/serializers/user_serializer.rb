class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :login, :chain

  def chain
    ActiveModelSerializers::SerializableResource.new(
      object.chain,
      serializer: ChainSerializer
    ) if object.chain.present?
  end
end
