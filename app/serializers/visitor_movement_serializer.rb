class VisitorMovementSerializer < ActiveModel::Serializer
  attributes :id, :created_at, :kind
end
