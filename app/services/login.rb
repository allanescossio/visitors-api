class Login < ApplicationService
  def initialize(user, password)
    @user = user
    @password = password
  end

  def call
    return OpenStruct.new(authenticated?: false, error: "Dados inválidos") if user.blank? || !user.authenticate(password)

    OpenStruct.new(authenticated?: true, token: new_token)
  end

  private

  attr_reader :user, :password

  def new_token
    payload = {
      user_id: user.id,
      token_type: 'client_a2',
      exp: 4.hours.from_now.to_i
    }
    JWT.encode payload, Rails.application.credentials[:secret_jwt_auth], 'HS256'
  end
end
