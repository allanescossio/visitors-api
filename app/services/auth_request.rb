class AuthRequest < ApplicationService
  def initialize(token)
    @token = token
  end

  def call
    return OpenStruct.new(authenticated?: false, error: "Efetue login") if token_decoded[:invalid]
    return OpenStruct.new(authenticated?: false, error: "Sessão expirada. Efetue o login novamente.") if token_decoded[:expired]

    OpenStruct.new(authenticated?: true, token: renewed_token)
  end

  private

  attr_reader :token

  def token_decoded
    @token_decoded ||= jwt_decode.first
  end

  def credential
    @credential ||= Rails.application.credentials[:secret_jwt_auth]
  end

  def jwt_decode
    begin
      JWT.decode token, credential, true, { :algorithm => 'HS256' }
    rescue JWT::ExpiredSignature
      [{ expired: true }]
    rescue
      [{ invalid: true }]
    end
  end

  def renewed_token
    token_decoded[:exp] = 4.hours.from_now.to_i
    JWT.encode token_decoded, credential, 'HS256'
  end
end
