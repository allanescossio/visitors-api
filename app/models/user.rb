class User < ApplicationRecord
  has_secure_password

  belongs_to :chain, optional: true

  validates_presence_of :name, :login, :password
end
