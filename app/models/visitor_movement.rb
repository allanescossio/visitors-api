class VisitorMovement < ApplicationRecord
  belongs_to :visitor

  enum kind: %i[entry arrival]
end
