class Visitor < ApplicationRecord
  searchkick

  belongs_to :chain

  has_many :visitor_movements, dependent: :destroy

  validates_presence_of :name

  mount_base64_uploader :photo, ImageUploader, file_name: -> (u) {"image#{Time.current.to_i.to_s}" }
end
