class Chain < ApplicationRecord
  searchkick
  
  has_many :users, dependent: :destroy
  has_many :visitors, dependent: :destroy

  validates_presence_of :name
  validates :cnpj, presence: true, length: { is: 14 }
end
