class CreateVisitorMovements < ActiveRecord::Migration[6.0]
  def change
    create_table :visitor_movements do |t|
      t.references :visitor, index: true, foreign_key: true
      t.integer :kind
      t.timestamps
    end
  end
end
