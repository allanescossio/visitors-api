class CreateChains < ActiveRecord::Migration[6.0]
  def change
    create_table :chains do |t|
      t.string :name, null: false
      t.string :cnpj, limit: 14, null: false
      t.timestamps
    end
    add_index :chains, :name, unique: true
    add_index :chains, :cnpj, unique: true
  end
end
