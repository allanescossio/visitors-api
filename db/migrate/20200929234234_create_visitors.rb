class CreateVisitors < ActiveRecord::Migration[6.0]
  def change
    create_table :visitors do |t|
      t.references :chain, index: true, foreign_key: true
      t.string :name, null: false
      t.timestamps
    end
  end
end
