# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_30_140614) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chains", force: :cascade do |t|
    t.string "name", null: false
    t.string "cnpj", limit: 14, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cnpj"], name: "index_chains_on_cnpj", unique: true
    t.index ["name"], name: "index_chains_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.bigint "chain_id"
    t.string "name", null: false
    t.string "login", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["chain_id"], name: "index_users_on_chain_id"
    t.index ["login"], name: "index_users_on_login", unique: true
  end

  create_table "visitor_movements", force: :cascade do |t|
    t.bigint "visitor_id"
    t.integer "kind"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["visitor_id"], name: "index_visitor_movements_on_visitor_id"
  end

  create_table "visitors", force: :cascade do |t|
    t.bigint "chain_id"
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "photo"
    t.index ["chain_id"], name: "index_visitors_on_chain_id"
  end

  add_foreign_key "users", "chains"
  add_foreign_key "visitor_movements", "visitors"
  add_foreign_key "visitors", "chains"
end
