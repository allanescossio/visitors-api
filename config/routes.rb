Rails.application.routes.draw do
  resources :visitor_movements, only: %i[index show create]
  resources :visitors
  resources :users
  resources :chains

  get "chains/name/search/:query", to: "chains_search_name#index"
  get "visitors/name/search/:query", to: "visitors_search_name#index"

  post "login", to: "login#create"

  get "chains_summary", to: "chains_summary#index"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
