# README

# Executando aplicação

* É necessário ter o ruby 2.6.6 e o postgres 12 instalado na máquina. Tambem é possível baixar a imagem da aplicação no
  Docker Hub executando o seguinte comando: "docker pull allanescossio/visitors-api"
* master.key: 74fae996255e33c36f6904ce16134a15
* Executar "rails db:setup" para criar a base de dados
* Executar "rails s -b 0.0.0.0" para iniciar o projeto

